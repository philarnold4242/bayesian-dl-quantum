import numpy as np
import os
import pyquil.api as api
from pyquil import get_qc
from HHL_Rigetti import get_hhl_2x2, verify_with_swap_test_2x2


if __name__ == '__main__':
    if not os.path.exists("output"):
        os.mkdir("output")
    if not os.path.exists("figures"):
        os.mkdir("figures")
    A = 0.5*np.array([[3, 1], [1, 3]])
    b = np.array([1., 0.])
    b /= np.linalg.norm(b)
    r = 4

    real_state = [0.9492929682, 0.3143928443]
    qubits = list(range(6))  # Specify actual qubit topology here
    hhl = get_hhl_2x2(A, b, r, qubits)
    swaptest = verify_with_swap_test_2x2(real_state, qubits)
    complete_circuit = hhl + swaptest
    qc = get_qc('6q-qvm')

    complete_circuit.wrap_in_numshots_loop(10000)
    results = qc.run(complete_circuit)
    N = len(results)
    results = np.array(results)
    np.savetxt('output/Rigetti_QPU', results)
    successful_postselection = results[results[:, 0] == 1]
    swap_test_after_succ_postselection = successful_postselection[:, 1]
    prob_swap_test_success = 1 - sum(swap_test_after_succ_postselection) / N
    print(prob_swap_test_success)
