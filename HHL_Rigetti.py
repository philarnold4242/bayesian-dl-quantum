import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pyquil.quil as pq
import scipy.linalg
import seaborn as sns
import warnings
from grove.qft.fourier import inverse_qft
from grove.alpha.phaseestimation.phase_estimation import controlled
from grove.alpha.arbitrary_state.arbitrary_state import create_arbitrary_state
from pyquil.gates import H, I, X, SWAP, CSWAP, CNOT, MEASURE
from pyquil.api._errors import QVMError
from math import log
from numpy import pi as π


###############################################################################
# Plotting parameters #########################################################
###############################################################################
sns.set()
sns.set_style('whitegrid', {'axes.grid': True})
matplotlib.rcParams['legend.shadow'] = True
matplotlib.rcParams['legend.frameon'] = True
plt.clf()
colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#9467bd', '#8c564b', ]
linestyles = ['solid', 'dashed', 'dotted', (0, (3, 1, 1, 1))]
textfontsize = 14
fontsize = 14


###############################################################################
# Gates for subroutines of the general algorithm ##############################
###############################################################################
def bwCNOT(c1, c2, t):
    '''Generate a circuit that implements a NOT gate in the target qubit if the
    first control is in state |1> and the second control is in state |0>.

    :param c1: (int) First control qubit.
    :param c2: (int) Second control qubit.
    :param t: (int) Target qubit.

    :return: A Quil program
    '''
    bwCNOT_mat = np.array([[1, 0, 0, 0, 0, 0, 0, 0],
                           [0, 1, 0, 0, 0, 0, 0, 0],
                           [0, 0, 1, 0, 0, 0, 0, 0],
                           [0, 0, 0, 1, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 1, 0, 0],
                           [0, 0, 0, 0, 1, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 1, 0],
                           [0, 0, 0, 0, 0, 0, 0, 1]])
    p = pq.Program()
    p.defgate('bwCNOT', bwCNOT_mat)
    p.inst(('bwCNOT', c1, c2, t))
    return p


def CNOTCNOT(c, t1, t2):
    '''Generate a circuit that implements a NOT gate each of the target qubits
    if the control is in state |1>.

    :param c: (int) Control qubit.
    :param t1: (int) First target qubit.
    :param t2: (int) Second target qubit.

    :return: A Quil program
    '''
    p = pq.Program()
    p.inst(CNOT(c, t1))
    p.inst(CNOT(c, t2))
    return p


def multiplewCNOT(controls, t):
    '''Generate a circuit that implements a NOT gate in the target qubit if all
    the control qubits are in state |0>.

    :param controls: (list of int) Control qubits.
    :param t: (int) Target qubit.

    :return: A Quil program
    '''
    w3CNOT_mat = np.eye(2**(len(controls) + 1))
    w3CNOT_mat[:2, :2] = np.array([[0, 1],
                                   [1, 0]])
    p = pq.Program()
    p.defgate('mwCNOT', w3CNOT_mat)
    p.inst(('mwCNOT', ) + controls + tuple((t, )))
    return p


def rY(angle):
    '''Generate a rotation matrix over the Y axis in the Bloch sphere.

    :param angle: (float) The angle of rotation.

    :return: (numpy.ndarray) The rotation matrix
    '''
    return np.array([[np.cos(angle/2), -np.sin(angle/2)],
                     [np.sin(angle/2), np.cos(angle/2)]])


###############################################################################
# Subroutines and general algorithm ###########################################
###############################################################################
def prepare(A, b, accuracy):
    '''Generate a circuit for preparing the initial state of the HHL algorithm
    and the unitary that will be applied to it.

    :param A: (numpy.ndarray) A Hermitian matrix.
    :param b: (numpy.ndarray) A vector.
    :param accuracy: (int) Number of bits of accuracy desired.

    :return p: A Quil program to perform state preparation.
    :return U: (numpy.ndarray) The unitary matrix exp(2*pi*i*A)
    '''
    assert isinstance(accuracy, int)
    U = scipy.linalg.expm(2j*np.pi*A)
    rows, cols = U.shape
    m = int(log(rows, 2))
    U_qubits = range(accuracy, accuracy + m)
    p = pq.Program()
    p.inst(create_arbitrary_state(b, U_qubits))
    return p, U


def phase_estimation(U, b, accuracy):
    '''Generate a circuit for the quantum phase estimation subroutine in HHL.

    :param A: (numpy.ndarray) A Hermitian matrix matrix.
    :param b: (numpy.ndarray) A vector.
    :param accuracy: (int) Number of bits of accuracy desired.

    :return p: A Quil program to perform phase estimation.
    :return output_qubits: (list of int) The qubits that encode the eigenvalues
    :return m: (int) Number of qubits that are needed to store the vector b.
    '''
    assert isinstance(accuracy, int)
    rows, cols = U.shape
    m = int(log(rows, 2))
    output_qubits = range(accuracy)
    U_qubits = range(accuracy, accuracy + m)
    p = pq.Program()
    # Hadamard initialization
    for i in output_qubits:
        p.inst(H(i))
    # Controlled unitaries
    for i, qubit in enumerate(output_qubits):
        if i > 0:
            U = np.dot(U, U)
        cU = controlled(U)
        name = 'CONTROLLED-U{0}'.format(2 ** i)
        # Define the gate
        p.defgate(name, cU)
        # Apply it
        p.inst((name, qubit) + tuple(U_qubits))

    # Compute the QFT
    p = p + inverse_qft(output_qubits)

    # Transform to right-to-left notation
    for i in range(int(accuracy/2)):
        p.inst(SWAP(output_qubits[i], output_qubits[-i-1]))

    return p, output_qubits, m


def eig_inversion(input_qubits, output_qubits):
    '''Generate a circuit for the eigenvalue inversion subroutine in HHL.

    :param input_qubits: (list of int) Qubits that encode the eigenvalues.
    :param output_qubits: (list of int) Qubits that will encode the reciprocals
                           of the eigenvalues.

    :return: A Quil program to perform eigenvalue inversion.
    '''
    assert len(input_qubits) == len(output_qubits), ('The number of input and'
                                                     + 'output qubits are not'
                                                     + 'the same')
    flag_qubit = max(output_qubits) + 1     # The flag qubit is the last one

    p = pq.Program()
    p += CNOTCNOT(input_qubits[0], flag_qubit, output_qubits[-1])
    for i, j in zip(input_qubits[1:], reversed(output_qubits[:-1])):
        p += bwCNOT(i, flag_qubit, j)
        p.inst(CNOT(j, flag_qubit))
    p += multiplewCNOT(tuple(output_qubits), flag_qubit)
    p.inst(X(flag_qubit))
    return p


def cRot(circuit, register_qubits):
    '''Generate a circuit for the conditional rotation subroutine in HHL.

    :param circuit: (pyquil.quil.Program) Program storing the HHL algorithm.
    :param register_qubits: (list of int) Qubits that encode the reciprocals of
                             the eigenvalues.

    :return p: A Quil program to perform conditional rotation.
    :return flag_qubit: (int) The qubit that stores the protocol success.
    '''
    flag_qubit = max(circuit.get_qubits()) + 1
    p = pq.Program()
    for i, qubit in enumerate(register_qubits):
        name = 'CONTROLLED-R{0}'.format(i)
        p.defgate(name, controlled(rY(2*π/2**i)))
        p.inst((name, qubit, flag_qubit))
    return p, flag_qubit


def get_complete_hhl(A, b, acc):
    '''Generate a circuit that implements the full HHL algorithm.

    :param A: (numpy.ndarray) A Hermitian matrix.
    :param b: (numpy.ndarray) A vector.
    :param accuracy: (int) Number of bits of accuracy desired.

    :return: A Quil program to perform HHL.
    '''
    prep, U = prepare(A, b, acc)
    pe, eig_qubits, n_b_qubits = phase_estimation(U, b, acc)
    ei = eig_inversion(eig_qubits, range(len(eig_qubits) + n_b_qubits,
                                              2*len(eig_qubits) + n_b_qubits))
    cr, cr_qubit = cRot(prep + pe + ei, range(len(eig_qubits) + n_b_qubits,
                                  2*len(eig_qubits) + n_b_qubits))
    c = cr.declare('ro', 'BIT', 1)
    cr += MEASURE(cr_qubit, c[0])
    return prep + pe + ei + cr + pe.dagger(), c


###############################################################################
# Special 2x2 algorithm #######################################################
###############################################################################
def get_hhl_2x2(A, b, r, qubits):
    '''Generate a circuit that implements the full HHL algorithm for the case
    of 2x2 matrices.

    :param A: (numpy.ndarray) A Hermitian 2x2 matrix.
    :param b: (numpy.ndarray) A vector.
    :param r: (float) Parameter to be tuned in the algorithm.
    :param verbose: (bool) Optional information about the wavefunction.

    :return: A Quil program to perform HHL.
    '''
    p = pq.Program()
    p.inst(create_arbitrary_state(b, [qubits[2]]))
    p.inst(H(qubits[0]))
    p.inst(H(qubits[1]))
    p.defgate('CONTROLLED-U0', controlled(scipy.linalg.expm(2j*π*A/4)))
    p.inst(('CONTROLLED-U0', qubits[1], qubits[2]))
    p.defgate('CONTROLLED-U1', controlled(scipy.linalg.expm(2j*π*A/2)))
    p.inst(('CONTROLLED-U1', qubits[0], qubits[2]))
    p.inst(SWAP(qubits[0], qubits[1]))
    p.inst(H(qubits[1]))
    p.defgate('CSdag', controlled(np.array([[1, 0], [0, -1j]])))
    p.inst(('CSdag', qubits[0], qubits[1]))
    p.inst(H(qubits[0]))
    p.inst(SWAP(qubits[0], qubits[1]))
    uncomputation = p.dagger()
    p.defgate('CRy0', controlled(rY(2*π/2**r)))
    p.inst(('CRy0', qubits[0], qubits[3]))
    p.defgate('CRy1', controlled(rY(π/2**r)))
    p.inst(('CRy1', qubits[1], qubits[3]))
    flag = p.declare('flag', 'BIT', 1)
    p += MEASURE(qubits[3], flag[0])
    p += uncomputation
    return p, flag


def verify_with_swap_test_2x2(reference_amplitudes, qubits):
    '''Generate a circuit for performing a swap test.

    :param program: (pyquil.quil.Program) Program storing the HHL algorithm.
    :param reference_amplitudes: (numpy.ndarray) Amplitudes of the state to be
                                  compared against.

    :return: A Quil program to do a swap test after inverting 2x2 matrices.
    '''
    swaptest = pq.Program()
    swaptest.inst(create_arbitrary_state(reference_amplitudes, [qubits[4]]))
    swaptest.inst(H(qubits[5]))
    swaptest.inst(CSWAP(qubits[5], qubits[4], qubits[3]))
    swaptest.inst(H(qubits[5]))
    swap = swaptest.declare('ro', 'BIT', 1)
    swaptest += MEASURE(qubits[5], swap[0])
    return swaptest, swap


###############################################################################
# Helper functions ############################################################
###############################################################################
def repeat_until_success(execution_engine, program,
                         then_program=None, conditional_bit=('ro', 0)):
    '''Helper function, returns the result of the program only if the
    postselection after the conditional rotation in HHL is successful.

    :param execution_engine: (pyquil.api.qvm.QVMConnection) Engine where the
                              program is run
    :param program: (pyquil.quil.Program) Program storing the HHL algorithm.
    :param then_program: (pyquil.quil.Program) Program to be run in case of
                          success.
    :param conditional_bit: (tuple (str, int)), or (classical register)
                            Classical memory address storing the success flag.

    :return run: (pyquil.wavefunction.Wavefunction or list)
                  Result of the execution.
    :return repetitions: (int) Number of repetitions until protocol success.
    '''
    if then_program is None:
        then_program = pq.Program(I(0))
    finished = False
    repetitions = 0
    while not finished:
        try:
            run = execution_engine(program.if_then(conditional_bit,
                                                   then_program))
            # If postselection succeeds, amplitudes for all states where the
            # flag qubit is |0> should be 0.
            # This is a simple hack. To work, the flag qubit must be the last.
            flag_zero_amplitudes = run.amplitudes[:int(len(run.amplitudes)/2)]
            postselection_succeeded = abs(sum(flag_zero_amplitudes)) < 1e-10
            max_repetitions_reached = repetitions > 100
            finished = postselection_succeeded or max_repetitions_reached
            if max_repetitions_reached:
                warnings.warn('Max unsuccessful repetitions reached')
            repetitions += 1
        except QVMError:
            pass
    return run, repetitions


def fidelity(amplitudes1, amplitudes2):
    '''Helper function, computes the fidelity of two pure quantum states given
    their amplitude vectors.
    
    :param amplitudes1: (array) Amplitudes of the first quantum state |psi1>
    :param amplitudes2: (array) Amplitudes of the second quantum state |psi2>
    
    :return: (float) the fidelity |<psi1|psi2>|^2
    '''
    return np.abs(np.dot(amplitudes1.conj(), amplitudes2))**2


def create_plot_fidelity(n, noise_types, noise_levels):
    '''Helper function, creates the plot of fidelity versus noise strength
    shown in Figures 1 and 2.

    :param n: (int) Size of the matrix that has been inverted.
    :param noise_types: (list of str) Noise types used during data acquisition.
    :param noise_levels: (list of int) Noise strengths used for the plots.

    :return: Corresponding plot.
    '''
    plt.clf()
    for noise_type in range(2):
        color = colors[noise_type]
        linestyle = linestyles[noise_type]
        data = np.loadtxt('output/' + noise_types[noise_type] + '_Fidelity_'
                          + n + 'x' + n)
        mean = data.mean(axis=0)
        variance = data.std(axis=0)
        plt.plot(noise_levels[noise_type], mean, label=noise_types[noise_type],
                 lw=1.1, color=color, linestyle=linestyle)
        zero = np.zeros(len(variance))
        ones = np.ones(len(variance))
        plt.fill_between(noise_levels[noise_type],
                         np.maximum(zero, mean - variance),
                         np.minimum(ones, mean + variance),
                         alpha=0.3, color=color)
        plt.ylabel('Fidelity', fontsize=textfontsize, rotation=90)
        plt.xlabel(r'Noise level', fontsize=textfontsize)
        plt.tick_params(axis='both', which='major', pad=3, labelsize=fontsize)
        leg = plt.legend(loc=[0.4, 0.7], fancybox=True,
                         shadow=True, fontsize=fontsize)
        leg.set_alpha(0.5)
        plt.grid(linewidth=0.5)
        plt.tight_layout()
    plt.savefig('figures/Fidelity' + n + 'x' + n + '.pdf')
    return plt

def create_plot_repetitions(n, noise_levels):
    '''Helper function, creates the plots of number of repetitions versus gate
    noise strength shown in Figures 1 and 2.

    :param n: (int) Size of the matrix that has been inverted.
    :param noise_levels: (list of int) Noise strengths used for the plots.

    :return: Corresponding plot.
    '''
    plt.clf()
    color = colors[0]
    linestyle = linestyles[0]
    data = np.loadtxt('output/Gate Noise_Repetitions_' + n + 'x' + n)
    mean = data.mean(axis=0)
    variance = data.std(axis=0)
    ones = np.ones(len(variance))
    plt.plot(noise_levels, mean, label='Average',
             lw=1.1, color=color, linestyle=linestyle)
    plt.fill_between(noise_levels,
                     np.maximum(ones, mean - variance),
                     mean + variance, 
                     alpha=0.3, color=color)
    plt.plot(noise_levels, np.max(data, axis=0), label='Worst case',
         lw=1.1, color=colors[1], linestyle=linestyles[1])
    plt.ylabel('Repetitions', fontsize=textfontsize, rotation=90)
    plt.xlabel(r'Gate noise level', fontsize=textfontsize)
    plt.tick_params(axis='both', which='major', pad=3, labelsize=fontsize)
    leg = plt.legend(loc=[0.05, 0.75], fancybox=True,
                     shadow=True, fontsize=fontsize)
    leg.set_alpha(0.5)
    plt.grid(linewidth=0.5)
    plt.tight_layout()
    plt.savefig('figures/Repetitions' + n + 'x' + n + '.pdf')
    return plt
